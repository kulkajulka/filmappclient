package pl.sda.filmappclient;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class AboutUsController {

    @RequestMapping(value = "/about")
    public String addAboutUsPage() {
        return "AboutUs";
    }
}
