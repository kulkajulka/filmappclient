package pl.sda.filmappclient;

import org.apache.log4j.Logger;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.filmappmodel.model.Actor;

import javax.validation.Valid;

import static org.apache.log4j.Logger.getLogger;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class ActorController {

    private static final Logger logger = getLogger(ActorController.class);

    @RequestMapping(value = "/actor")
    public String addActorPage(Actor actor) {
        return "actorform";
    }

    @PostMapping("/actor")
    public String actorSubmit(@ModelAttribute @Valid Actor actor) {
        logger.info("We should post actor to server through for example restTemplate: " + actor);
        return "actorresult";
    }

    @RequestMapping("/actorlist")
    public String actorList(Model model) {
        logger.info("We should retrieve all actors from DB through server");
        setMockedUserList(model);
        return "actorlist";
    }

    private void setMockedUserList(Model model) {
        List<Actor> actorList = new ArrayList<>();
        Actor actor1 = new Actor(1, "Piotr", "Sabacinski", new Date(1990, 10, 10), null, 120.0);
        Actor actor2 = new Actor(2, "Hela", "Polanski", null, null, 10.0);
        actorList.add(actor1);
        actorList.add(actor2);
        model.addAttribute("actorList", actorList);
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy/MM/dd"), true));
    }

}
