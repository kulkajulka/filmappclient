package pl.sda.filmappclient;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.filmappmodel.model.Film;
import pl.sda.filmappmodel.model.Genre;

import java.util.ArrayList;
import java.util.List;

import static org.apache.log4j.Logger.getLogger;

@Controller
public class FilmController {

    private static final Logger logger = getLogger(FilmController.class);

    @RequestMapping("/film")
    public String filmForm(Film film) {
        return "filmform";
    }

    @PostMapping("/film")
    public String filmSubmit(@ModelAttribute Film film) {
        logger.info("We should post user to server through for example restTemplate: " + film);
        return "filmresult";
    }

    @RequestMapping("/filmlist")
    public String filmList(Model model) {
        mockFilmList(model);
        return "films";
    }

    private void mockFilmList(Model model) {
        List<Film> filmList = new ArrayList<>();
        Genre dramat = new Genre(12, "Drama");
        Genre western = new Genre(15, "Western");
        Film film1 = new Film("Quo Vadis", "Jerzy Kawalerowicz", 2001, dramat, "7.8", null, null, null);
        Film film2 = new Film("Dzień Świra", "Marek Koterski", 2002, western, "8.2", null, null, null);
        filmList.add(film1);
        filmList.add(film2);
        model.addAttribute("filmlist", filmList);
    }
}
