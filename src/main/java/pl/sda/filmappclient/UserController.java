package pl.sda.filmappclient;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import pl.sda.filmappmodel.model.User;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.util.Arrays.asList;
import static org.apache.log4j.Logger.getLogger;
import static pl.sda.filmappmodel.model.UserRole.USER;

@Controller
public class UserController {

    private static final Logger LOGGER = Logger.getLogger(UserController.class);

    RestTemplate restTemplate = new RestTemplate();

    @Value("${mock.data}")
    private boolean mockData = true;

    @Value("${filmapp.server.url}")
    private String filmappHost;

    @Value("${filmapp.server.port}")
    private String filmappPort;

    @Value("${filmapp.server.url.user.users}")
    private String usersPath;

    private static final Logger logger = getLogger(UserController.class);

    @RequestMapping("/user")
    public String userForm(User user) {
        return "userform";
    }

    @PostMapping("/user")
    public String userSubmit(@ModelAttribute User user) throws JsonProcessingException {
        logger.info("We should post user to server through for example restTemplate: " + user);
        restTemplate.postForObject(filmappHost + filmappPort + "/user/signup", user, ResponseEntity.class);
        return "userresult";
    }

    @RequestMapping("/userlist")
    public String userList(Model model) {
        logger.info("We should retrieve all users from DB through server");
        if (mockData) {
            model.addAttribute("userList", createMockUsers());
        } else {
            LOGGER.info(filmappHost + filmappPort + usersPath);
            User[] users = restTemplate.getForObject(filmappHost + filmappPort + usersPath, User[].class);
            model.addAttribute("userList", asList(users));
        }
        return "userlist";
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
    }

    private List<User> createMockUsers() {
        List<User> users = new ArrayList<>();
        User user1 = new User(1, "HugoB", "12345", "Hugo", "Boss", "null", new Date(), USER);
        User user2 = new User(2, "BobbyB", "11111", "Bobby", "Burger", "null", new Date(1999, 10, 10), USER);
        users.add(user1);
        users.add(user2);
        return users;
    }
}
