package pl.sda.filmappclient;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class FilmappClientApplicationTests {

	@Autowired
	MockMvc mockMvc;

	@Test
	public void aboutUsPageTest200() throws Exception {
		mockMvc.perform(
				get("/about")).
				andExpect(status().is(200));
	}

	@Test
	public void actorPageTest200() throws Exception {
		mockMvc.perform(
				get("/actor")).
				andExpect(status().is(200));
	}
	@Test
	public void userPageTest200() throws Exception {
		mockMvc.perform(
				get("/user")).
				andExpect(status().is(200));
	}
	@Test
	public void filmPageTest200() throws Exception {
		mockMvc.perform(
				get("/film")).
				andExpect(status().is(200));
	}
	@Test
	public void filmListPageTest200() throws Exception {
		mockMvc.perform(
				get("/filmlist")).
				andExpect(status().is(200));
	}
	@Test
	public void actorListPageTest200() throws Exception {
		mockMvc.perform(
				get("/actorlist")).
				andExpect(status().is(200));
	}
	@Test
	public void contextLoads() {
	}
}